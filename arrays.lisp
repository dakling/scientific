(in-package :scientific)

(defmethod length ((arg1 grid:vector-double-float))
  (reduce #'* (grid:dimensions arg1)))

(defun foreign-array-p (object)
    (typep object 'grid:foreign-array))

(defun list->foreignvector (&rest arg)
  (grid:make-foreign-array
   'double-float
   :initial-contents
   (map (lambda (x) (* 1d0 x))
        (if (and (= (length arg) 1) (listp (car arg)))
            (car arg)
            arg))))

(defun foreignvector->list (arg)
  (loop for i from 0 below (length arg)
        collecting (gref arg i)))

(defmethod g-to-list ((arg grid:vector-double-float))
  (foreignvector->list arg))

(defmethod map (fn (arg1 grid:vector-double-float) &rest other-args)
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (if other-args
                       (apply fn (gref arg1 i) (map (lambda (x) (gref x i)) other-args))
                       (funcall fn (gref arg1 i)))))
    out))

(defmethod add ((arg1 grid:vector-double-float) (arg2 grid:vector-double-float))
  (assert (= (length arg1) (length arg2)) nil "Length mismatch detected.")
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (+
                    (gref arg1 i)
                    (gref arg2 i))))
    out))

(defmethod add ((arg1 grid:vector-double-float) (arg2 list))
  (assert (= (length arg1) (length arg2)) nil "Length mismatch detected.")
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (+
                    (gref arg1 i)
                    (gref arg2 i))))
    out))

(defmethod add ((arg2 list) (arg1 grid:vector-double-float))
  (add arg1 arg2))

(defmethod add ((arg1 grid:vector-double-float) (arg2 number))
  (let ((out (grid:make-foreign-array 'double-float)))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (+
                    (gref arg1 i)
                    arg2)))
    out))

(defmethod add ((arg2 number) (arg1 grid:vector-double-float))
  (add arg1 arg2))

(defmethod multiply ((arg1 grid:vector-double-float) (arg2 grid:vector-double-float))
  (assert (= (length arg1) (length arg2)) nil "Length mismatch detected.")
  (reduce #'+
          (loop for i from 0 below (length arg1)
                collecting
                (*
                 (gref arg1 i)
                 (gref arg2 i)))))

(defmethod multiply ((arg1 grid:vector-double-float) (arg2 list))
  (assert (= (length arg1) (length arg2)) nil "Length mismatch detected.")
  (reduce #'+
          (loop for i from 0 below (length arg1)
                collecting
                (*
                 (gref arg1 i)
                 (gref arg2 i)))))

(defmethod multiply ((arg2 list) (arg1 grid:vector-double-float))
  (multiply arg1 arg2))

(defmethod multiply ((arg1 grid:vector-double-float) (arg2 number))
  (let ((out (grid:make-foreign-array 'double-float)))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (*
                    (gref arg1 i)
                    arg2)))
    out))

(defmethod multiply ((arg2 number) (arg1 grid:vector-double-float))
  (multiply arg1 arg2))

(defmethod negate ((arg1 grid:vector-double-float))
  (map (lambda (x) (negate x)) arg1))

(defmethod subtract ((arg1 grid:vector-double-float) (arg2 grid:vector-double-float))
  (assert (= (length arg1) (length arg2)) nil "Length mismatch detected.")
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (-
                    (gref arg1 i)
                    (gref arg2 i))))
    out))

(defmethod subtract ((arg1 grid:vector-double-float) (arg2 list))
  (assert (= (length arg1) (length arg2)) nil "Length mismatch detected.")
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (-
                    (gref arg1 i)
                    (gref arg2 i))))
    out))

(defmethod subtract ((arg2 list) (arg1 grid:vector-double-float))
  (negate (subtract arg1 arg2)))

(defmethod subtract ((arg1 grid:vector-double-float) (arg2 number))
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (-
                    (gref arg1 i)
                    arg2)))
    out))

(defmethod subtract ((arg2 number) (arg1 grid:vector-double-float))
  (negate (subtract arg1 arg2)))

(defmethod divide ((arg1 grid:vector-double-float) (arg2 grid:vector-double-float))
  (assert (= (length arg1) (length arg2)) nil "Length mismatch detected.")
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (/
                    (gref arg1 i)
                    (gref arg2 i))))
    out))

(defmethod divide ((arg1 grid:vector-double-float) (arg2 list))
  (assert (= (length arg1) (length arg2)) nil "Length mismatch detected.")
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (/
                    (gref arg1 i)
                    (gref arg2 i))))
    out))

(defmethod divide ((arg2 list) (arg1 grid:vector-double-float))
  (map (lambda (x) (/ x)) (subtract arg1 arg2)))

(defmethod divide ((arg1 grid:vector-double-float) (arg2 number))
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (/
                    (gref arg1 i)
                    arg2)))
    out))

(defmethod divide ((arg2 number) (arg1 grid:vector-double-float))
  (map (lambda (x) (/ x)) (divide arg1 arg2)))

(defmethod expt ((arg1 grid:vector-double-float) arg2)
  (let ((out (grid:make-foreign-array
              'double-float
              :dimensions (length arg1))))
    (loop for i from 0 below (length arg1)
          do (setf (grid:aref out i)
                   (expt
                    (gref arg1 i)
                    (* 1d0 arg2))))
    out))

(defmethod magnitude ((arg grid:vector-double-float))
  (sqrt (* arg arg)))

(defmethod gref ((arg1 grid:vector-double-float) index1 &rest indices)
  (apply #'grid:aref arg1 index1 indices))

(defmethod gapply (func (arg grid:vector-double-float) &rest other-args)
  (if other-args
      (if (cdr other-args)
          (apply func arg other-args)
          (apply func arg (car other-args)))
      (apply func (foreignvector->list arg))))

;; from https://stackoverflow.com/questions/9549568/common-lisp-convert-between-lists-and-arrays
(defun array-to-list (array)
  (let* ((dimensions (array-dimensions array))
         (depth      (1- (length dimensions)))
         (indices    (make-list (1+ depth) :initial-element 0)))
    (labels ((recurse (n)
               (loop for j below (nth n dimensions)
                     do (setf (nth n indices) j)
                     collect (if (= n depth)
                                 (apply #'aref array indices)
                               (recurse (1+ n))))))
      (recurse 0))))


(defmethod g-to-list ((arg array))
  (array-to-list arg))

(defmethod gref ((arg1 array) index1 &rest indices)
  (let ((rank (array-rank arg1)))
    (if (= rank (1+ (length indices)))
        (apply #'aref arg1 index1 indices)
        ;; TODO convert back to array
        (apply #'gref (g-to-list arg1) index1 indices))))

;; (defmethod lessp ((arg1 array) (arg2 number))
;;   (map (lambda (x) (max x arg2)) arg1))
