(in-package :scientific)

;; define some generic functions and their default implementation (for numbers)
;; by convention, the names are the usual names prefixed with a g
;; binary versions have a -bin suffix and are not meant to be used by the user, however,
;; they are neccessary to properly dispatch on the methods.

(defgeneric inv (arg1))

(defmethod inv ((arg1 number))
  (/ arg1))

(defgeneric gref (arg1 n &rest other-ns))

(defmethod gref ((arg1 number) n &rest other-ns)
  (if (and (= 0 n) (not other-ns))
      arg1
      (error "Trying to access second element of a scalar")))

(defgeneric gapply (func arg &rest other-args))

(defmethod gapply (func (arg symbol) &rest other-args)
  (if other-args
      (gapply func (gcons arg other-args))
      (funcall func arg)))

(defmethod gapply (func (arg number) &rest other-args)
  (if other-args
      (gapply func (gcons arg other-args))
      (funcall func arg)))

(defgeneric magnitude (arg1))

(defmethod magnitude ((arg1 number))
  (abs arg1))

(defgeneric gcons (arg1 arg2))

(defgeneric compatible-p (arg1 arg2))

(defun gbutlast (arg)
  (subseq arg 0 (1- (length arg))))

(defun grest (arg)
 (subseq arg 1))

(defun gdrop-right (arg n)
  (subseq arg 0 (- (length arg) n)))

(defun gdrop-left (arg n)
  (subseq arg n))

(defgeneric g-transpose (arg))

(defgeneric rectangular-p (arg))

(defgeneric gsplit (lst parts))

(defgeneric g-flatten (lst))

(defmethod g-flatten (arg)
  arg)

(defgeneric g-to-list (arg))

(defmethod g-to-list ((arg number))
  (list arg))

(defgeneric g-to-vector (arg))

(defmethod g-to-vector ((arg number))
  (vector arg))

(defmethod length ((arg number))
  1)
