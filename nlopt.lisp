(in-package :scientific)

(defvar *nlopt-current-result* nil)

(defun perturb-randomly (guess &optional (factor 1d-3))
  (+ guess (* factor (/ (random 1000) 1d3))))

(defun newton (func initial-guess &key (tolerance 1d-6) (max-iter 10000) (write-interval 10))
  (labels
      ((iter (current-guess old-guess old-residual number-of-iterations step-size-reduction-factor)
         (let*
             ((fn (gapply func (setf *nlopt-current-result* current-guess)))
              (current-residual (l2-norm fn)))
           (when (and write-interval (= 0 (mod number-of-iterations write-interval)))
            (format t "Newton iteration number ~A, residual: ~A, step size reduction factor: ~A ~&"
                    number-of-iterations
                    current-residual
                    step-size-reduction-factor))
           (cond
             ((and old-guess old-residual (> current-residual old-residual))
              (progn
                (format t "Residual increased, going back and reducing step size ~&")
                (if (> 1d-6 step-size-reduction-factor)
                    (progn
                      (format t "Step size is too low, randomly perturbing current guess ~&")
                      (iter (perturb-randomly current-guess step-size-reduction-factor) nil nil (1+ number-of-iterations) 1d-6))
                    (iter
                      old-guess
                      nil
                      nil
                      (1+ number-of-iterations)
                      (/ step-size-reduction-factor 4d0)))))
             ((> tolerance current-residual)
              (progn
                (format t "Newton converged in ~A iterations ~&" number-of-iterations)
                (values current-guess current-residual)))
             ((and max-iter (> number-of-iterations max-iter))
              (progn
                (format t "Newton failed to converge in ~A iterations" max-iter)
                (values current-guess current-residual))
              ;; (error "Newton failed to converge in ~A iterations" max-iter)
              )
             (t
              (let*
                  ((jac (jacobian-fds func current-guess))
                   (delta-y (ignore-errors (/ fn jac))))
                (format t "condition number: ~A ~&" (py-condition-number jac))
                (if delta-y
                    (iter
                      (-
                          current-guess
                          (* 1d-1 step-size-reduction-factor delta-y))
                      current-guess
                      current-residual
                      (1+ number-of-iterations)
                      (min 1d0 (* 2d0 step-size-reduction-factor)))
                    (progn (format t "Singular matrix encountered after ~A iterations, randomly perturbing current guess ~&" number-of-iterations)
                           (iter (perturb-randomly current-guess) nil nil (1+ number-of-iterations) step-size-reduction-factor)))))))))
    (iter initial-guess nil nil 0 1d0)))

(defun multimin-no-derivative
    (fn &key
          (method gsll:+simplex-nelder-mead-on2+)
          (write-interval nil)
          (tolerance 1.0d-3)
          (max-iterations 100)
          (dimensions 2)
          (initial-guess (make-list dimensions :initial-element 0)))
  "finds local minima of the given function using gsl bindings"
  (let ((step-size (grid:make-foreign-array 'double-float :dimensions dimensions)))
    (gsll:set-all step-size 1.0d-3)
    (let ((minimizer
            (gsll:make-multi-dimensional-minimizer-f
             method dimensions fn
             (grid:make-foreign-array
              'double-float :initial-contents initial-guess)
             step-size)))
      (loop with status = T and gsll:size
            for iter from 0 below max-iterations
            while status
            do (gsll:iterate minimizer)
               (setf step-size
                     (gsll:size minimizer)
                     status
                     (not (gsll:min-test-size step-size tolerance))
                     *nlopt-current-result* (gsll:solution minimizer))
               (when (and write-interval (= 0 (mod iter write-interval)))
                 (let ((x (gsll:solution minimizer)))
                   (format t "iteration: ~A residual:~12,9f~&"
                           iter (gsll:function-value minimizer))))
            finally
               (return
                 (values
                  (let ((x (gsll:solution minimizer)))
                    (list (loop for n from 0 to (- dimensions 1) collect (grid:aref x n)) (gsll:function-value minimizer)))
                  (gsll:function-value minimizer)))))))

(defun multimin-with-derivative
    (fn &key
          (method gsll:+vector-bfgs2+)
          (write-interval nil)
          (tolerance 1.0d-3)
          (max-iterations 100)
          (dimensions 1)
          (initial-guess (make-list dimensions :initial-element 0)))
  (let
      ((step-size (grid:make-foreign-array 'double-float :dimensions dimensions)))
    (gsll:set-all step-size 1.0d0)
    (let ((minimizer
            (labels
                ((df (x out)
                   (loop for i from 0 below dimensions
                         do (setf (grid:aref out i) (gref
                                                     (diff (lambda (&rest x)
                                                             (funcall fn x))
                                                           x i)
                                                     0))))
                 (fdf (arg-ptr val-ptr df-ptr)
                   (prog1
                       (setf (grid:aref val-ptr 0) (* 1d0 (funcall fn arg-ptr)))
                     (df arg-ptr df-ptr))))
              (gsll:make-multi-dimensional-minimizer-fdf
               method dimensions (list fn #'df #'fdf)
               (grid:make-foreign-array
                'double-float :initial-contents initial-guess)
               1d-3
               1d-1
               nil))))
      (loop with status = T
            for iter from 0 below max-iterations
            while status
            do
               (gsl:iterate minimizer)
               (setf status
                     (not (gsl:min-test-gradient
                           (gsl:mfdfminimizer-gradient minimizer)
                           tolerance))

                     *nlopt-current-result* (gsll:solution minimizer))
               (when (and write-interval (= 0 (mod iter write-interval)))
                 (let ((x (gsl:solution minimizer)))
                   (format t "iteration: ~A residual:~12,9f~&"
                           iter (gsl:function-value minimizer))))
            finally
               (return
                 (values
                  (let ((x (gsl:solution minimizer)))
                    (foreignvector->list x))
                  (gsl:function-value minimizer)))))))

(defun multiroot-no-derivative
    (fn &key
          (method gsl:+hybrid-scaled+)
          (write-interval nil)
          (tolerance 1.0d-5)
          (max-iter 1000)
          (dimensions 2)
          (initial-guess (make-list dimensions :initial-element 0)))
  "finds local minima of the given function using gsl bindings"
  (progn
    ;; (format t "creating solver~&")
    (let*
        ((number-of-eqs (length initial-guess))
         (solver
           (gsl:make-multi-dimensional-root-solver-f
            method
            ;; fn
            (lambda (x out)
              (let ((res (funcall fn x)))
               (progn
                 (loop for i from 0 below number-of-eqs
                       do (setf (grid:aref out i) (gref res i)))
                 out)))
            initial-guess
            nil)))
      (progn
        ;; (format t "starting loop~&")
        (loop for iter from 0
              with fnval and argval
              while (and (< iter max-iter)
                         (or (zerop iter)
                             (not (gsll:multiroot-test-residual solver tolerance))))
              do
                 (gsll:iterate solver)
                 (setf fnval (gsll:function-value solver)
                       argval (gsll:solution solver)
                       *nlopt-current-result* argval)
                 (when (and write-interval (= 0 (mod iter write-interval)))
                   (format t "iteration ~d, residual per DOF: ~A~&"
                           iter
                           (/ (magnitude fnval) (length fnval))))
              finally (return
                        (values
                         (foreignvector->list argval)
                         (/ (magnitude fnval) (length fnval)))))))))

(defun nelder-mead (func initial-guess &key (tolerance 1d-6) (max-iter 10000) (write-interval 10))
  (multimin-no-derivative
   (lambda (&rest x) (magnitude (apply func x)))
   :initial-guess (g-to-list initial-guess)
   :tolerance tolerance
   :max-iterations max-iter
   :dimensions (length initial-guess)
   :write-interval write-interval))

(defun broyden (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (multimin-with-derivative
   (lambda (x) (magnitude (gapply func x)))
   :method gsll:+vector-bfgs2+
   :initial-guess (g-to-list initial-guess)
   :tolerance tolerance
   :max-iterations max-iter
   :dimensions (length initial-guess)
   :write-interval write-interval))

(defun fletcher-reeves (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (multimin-with-derivative
   (lambda (x) (magnitude (gapply func x)))
   :method gsll:+conjugate-fletcher-reeves+
   :initial-guess (g-to-list initial-guess)
   :tolerance tolerance
   :max-iterations max-iter
   :dimensions (length initial-guess)
   :write-interval write-interval))

(defun hybrid-scaled (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (multiroot-no-derivative
   ;; (lambda (&rest x) (apply #'values (g-to-list (apply func x))))
   (lambda (x) (apply func (g-to-list x)))
   :method gsll:+hybrid-scaled+
   :initial-guess (list->foreignvector (g-to-list initial-guess))
   :tolerance tolerance
   :max-iter max-iter
   :dimensions (length initial-guess)
   :write-interval write-interval))

(defun hybrid-unscaled (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (multiroot-no-derivative
   ;; (lambda (&rest x) (apply #'values (g-to-list (apply func x))))
   (lambda (x) (apply func (g-to-list x)))
   :method gsll:+hybrid-unscaled+
   :initial-guess (list->foreignvector (g-to-list initial-guess))
   :tolerance tolerance
   :max-iter max-iter
   :dimensions (length initial-guess)
   :write-interval write-interval))

(defun discrete-newton (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (multiroot-no-derivative
   ;; (lambda (&rest x) (apply #'values (g-to-list (apply func x))))
   (lambda (x) (apply func (g-to-list x)))
   :method gsll:+discrete-newton+
   :initial-guess (list->foreignvector (g-to-list initial-guess))
   :tolerance tolerance
   :max-iter max-iter
   :dimensions (length initial-guess)
   :write-interval write-interval))

(defun simulated-annealing (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (flet
      ((energy-func (state)
         (let ((x (grid:aref state 0)))
           (declare (type double-float x) (optimize (speed 3) (safety 1)))
           (funcall func x)))
       (step-func (rng-mpointer state step-size)
         (declare
          (type double-float step-size) (optimize (speed 3) (safety 1))
          ;; Ignore the RNG foreign pointer because it doesn't do us
          ;; much good, we want the CL object which we'll get from
          ;; the dynamical environment.
          (ignore rng-mpointer)
          (special cl-generator))
         (symbol-macrolet ((x (grid:aref state 0)))
           (let ((rand (gsl:sample cl-generator :uniform)))
             (declare (type double-float rand))
             (setf x (+  (the double-float x) (- (* 2.0d0 rand step-size) step-size))))))
       (metric (state1 state2)
         (declare (optimize (speed 3) (safety 1)))
         (abs (- (the double-float (grid:aref state1 0))
                 (the double-float (grid:aref state2 0))))))
      (gsll:simulated-annealing
       ;; initial-guess
       (list 15.5d0)
       200 1000 1.0d0 1.0d0 0.008d0 1.003d0 2.0d-6 ; parameters
       (gsl:make-random-number-generator gsl:+mt19937+ 0)
       (lambda (&optional initial)
         ;; (grid:make-foreign-array 'double-float :dimensions 1 :initial-contents 1d0)
         (grid:make-foreign-array 'double-float :dimensions 1 :initial-contents initial)
         )
       ;; the energy function
       ;; 'energy-func
       'gsl::trivial-example-energy
       ;; the step function
       ;; 'step
       'gsl::trivial-example-step
       ;; the metric function
       ;; 'metric
       'gsl::trivial-example-metric
       'grid:copy)))

;; (gsll:simulated-annealing
;;        ;; initial-guess
;;        (list 15.5d0)
;;        200 1000 1.0d0 1.0d0 0.008d0 1.003d0 2.0d-6 ; parameters
;;        (gsl:make-random-number-generator gsl:+mt19937+ 0)
;;        (lambda (&optional (initial (list 1d0)))
;;          ;; (grid:make-foreign-array 'double-float :dimensions 1 :initial-contents (list 1d0))
;;          (grid:make-foreign-array 'double-float :dimensions 1 :initial-contents initial)
;;          )
;;        ;; the energy function
;;        ;; 'energy-func
;;        'gsl::trivial-example-energy
;;        ;; the step function
;;        ;; 'step
;;        'gsl::trivial-example-step
;;        ;; the metric function
;;        ;; 'metric
;;        'gsl::trivial-example-metric
;;        'grid:copy)

;; (simulated-annealing
;;  (lambda (x)
;;    (- (expt (gref x 0) 2) 2d0))
;;  (list 15.5d0))

;; (hybrid-scaled
;;  (lambda (x)
;;    (- (expt x 2) 2d0))
;;  1d0)

;; (hybrid-scaled
;;  (lambda (x)
;;    (- (expt x 2) 2))
;;  (ones 1))

;; (hybrid-scaled
;;  ;; fletcher-reeves
;;  (lambda (x y)
;;    (list
;;     (- (expt x 2) 2)
;;     (- (expt y 2) 3)))
;;  (list->foreignvector 1d0 4d0))

;; (hybrid-scaled
;;  ;; fletcher-reeves
;;  (lambda (&rest xs)
;;    (list
;;     (- (expt (gref xs 0) 2) 2)
;;     (- (expt (gref xs 1) 2) 3)))
;;  (list->foreignvector 1d0 4d0))

;; (let ((inp (sc:mvector->list (sc:make-uniform-grid 0d0 1d0 20000))))
;;   (apply #'values (sc::g-to-list (apply
;;                                   (lambda (&rest xs)
;;                                     (map
;;                                      (lambda (x) x)
;;                                      xs))
;;                                   inp))))
;; (gsl:make-multi-dimensional-root-solver-f
;;  gsll:+hybrid-scaled+
;;  (lambda (&rest x) (apply #'values (g-to-list x)))
;;  (grid:make-foreign-array 'double-float
;;                           :initial-contents
;;                           (loop for i from 0 to 100
;;                                 collecting i)))

;; (funcall (lambda (&rest x) (apply #'values (g-to-list x)))
;;          (grid:make-foreign-array 'double-float
;;                                   :initial-contents
;;                                   (loop for i from 0 to 10000
;;                                         collecting i)))


;; (gsl:multi-dimensional-minimizer-f)

;; (setf (slot-value *tst* 'gsl-vector)
;;       (sc::list->foreignvector (sc:mvector->list (sc:make-uniform-grid 0d0 1d0 30))))

;; (mapcar #'slot-definition-name
;;         (class-direct-slots (class-of *tst*)))

(py4cl:import-module "scipy.optimize" :as "pyopt" :reload t)

(defun scipy-root (func initial-guess &key (tolerance 1d-3) (max-iter 1000) (write-interval 10) (method "hybr"))
 (let*
     ((current-residual nil)
      (opt-result
        (py4cl:remote-objects
          (pyopt:root
           (lambda (x)
             (apply func (g-to-list x)))
           (g-to-vector initial-guess)
           ;; :tol tolerance
           :method method
           :options (alist->hash-table
                     (list
                      (cons "maxfev" max-iter)))
           :callback (when write-interval
                       (let ((iter 0))
                         (lambda (x f)
                           (incf iter)
                           (when (= 0 (mod iter write-interval))
                             (setf *nlopt-current-result* x)
                             (setf current-residual (magnitude f))
                             (format t "Iteration: ~A; Current residual: ~A ~&" iter current-residual)))))))))
   (values (py4cl:python-method opt-result "get" "x") current-residual)
   ;; (if (py4cl:python-method opt-result "get" "success")
   ;;     (values (py4cl:python-method opt-result "get" "x") current-residual)
   ;;     (progn
   ;;       (setf *nlopt-current-result* (py4cl:python-method opt-result "get" "x"))
   ;;       (error "Scipy solver failed to converge")))
   ))

(defun scipy-hybr (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (scipy-root func initial-guess
              :tolerance tolerance
              :max-iter max-iter
              :write-interval write-interval
              :method "hybr"))

(defun scipy-krylov (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (scipy-root func initial-guess
              :tolerance tolerance
              :max-iter max-iter
              :write-interval write-interval
              :method "krylov"))

(defun scipy-broyden (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (scipy-root func initial-guess
              :tolerance tolerance
              :max-iter max-iter
              :write-interval write-interval
              :method "broyden1"))

(defun scipy-df-sane (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (scipy-root func initial-guess
              :tolerance tolerance
              :max-iter max-iter
              :write-interval write-interval
              :method "df-sane"))

(defun scipy-anderson (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (scipy-root func initial-guess
              :tolerance tolerance
              :max-iter max-iter
              :write-interval write-interval
              :method "anderson"))

(defun scipy-levenberg-marquardt (func initial-guess &key (tolerance 1d-3) (max-iter 100) (write-interval 10))
  (scipy-root func initial-guess
              :tolerance tolerance
              :max-iter max-iter
              :write-interval write-interval
              :method "lm"))

;; (scipy-krylov
;;  (lambda (x)
;;    (- (expt x 2) 2d0))
;;  1d0)

;; (scipy-root
;;  (lambda (x)
;;    (- (expt x 2) 2d0))
;;  1d0)

;; (scipy-root
;;  (lambda (x y z)
;;    (list
;;     (- (expt x 2) 2)
;;     (- (expt z 2) 4)
;;     (- (expt y 2) 3)))
;;  (ones 3))

(defun mgl-minimize (func
                     initial-guess
                     &key (tolerance 1d-3)
                       (max-iter 1000)
                       (write-interval 10)
                       (method 'mgl:cg-optimizer))
  (declare (ignore write-interval tolerance))
  (g-to-list
   (mgl:minimize
    (make-instance method :termination max-iter
                          :batch-size 10)
    (make-instance 'mgl-diffun:diffun
                   :fn func
                   :weight-indices (a:iota (length initial-guess)))
    :weights (list->mgl-mat (g-to-list initial-guess)))))

(defun mgl-cg-minimize (func
                        initial-guess
                        &key (tolerance 1d-3)
                          (max-iter 1000)
                          (write-interval 10))
  (declare (ignore write-interval tolerance))
  (mgl-minimize
   func
   initial-guess
   :max-iter max-iter
   :method 'mgl:cg-optimizer))

(defun mgl-sgd-minimize (func
                         initial-guess
                         &key (tolerance 1d-3)
                           (max-iter 1000)
                           (write-interval 10))
  (declare (ignore write-interval tolerance))
  (mgl-minimize
   func
   initial-guess
   :max-iter max-iter
   :method 'mgl:sgd-optimizer))

;; (mgl-minimize
;;  (lambda (x)
;;    ;; (sin x)
;;    (expt (- (expt x 2) 2d0) 2))
;;  1.0d0
;;  ;; :method 'mgl:sgd-optimizer
;;  )

;; (mgl-minimize
;;  (lambda (x y z)
;;    (magnitude
;;     (list
;;      (- (expt x 2) 2)
;;      (- (expt y 2) 3)
;;      (- (expt z 2) 4))))
;;  (list 1 1 1)
;;  ;; :method 'mgl:sgd-optimizer
;;  ;; :max-iter 1000
;;  )

;; (mgl-minimize
;;  (lambda (&rest xs)
;;    (let
;;        ((x (nth 0 xs))
;;         (y (nth 1 xs))
;;         (z (nth 2 xs)))
;;      (magnitude
;;       (list
;;        (- (expt x 2) 2)
;;        (- (expt y 2) 3)
;;        (- (expt z 2) 4)))))
;;  (list 1 1 1)
;;  ;; :method 'mgl:sgd-optimizer
;;  ;; :max-iter 1000
;;  )
