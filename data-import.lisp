(in-package :scientific)

(defun empty-strin-p (str)
  "Check if a given str contains nothing or only whitespace"
  (string= (remove #\Space str) ""))

(defun not-empty-strin-p (str)
  "Check if a given str contains something other than whitespaces"
  (not (empty-strin-p str)))

(defun headerp (row)
  "check if row is a header by checking if it contains #"
  (defun contains-hash-p (str)
    (not (null (find #\# str))))
  (cond ((null row) nil)
        ((contains-hash-p (car row)) t)
         ((not(contains-hash-p (car row))) (headerp (cdr row)))))

(defun read-file (path &optional (separator #\,))
  "reads in a space-separated csv file"
  (cl-csv:read-csv path :separator separator))

(defun remove-headers (x)
  "remove all entries from a parsed csv-file that are headers"
  (remove-if #'headerp x))

(defun parse-number (str)
  "turn a string into a number if possible, else return nil"
  (cond ((empty-strin-p str) '())
        ((let ((num (with-input-from-string (in str) (read in))))
           (if (numberp num)
               num
               '())))))

(defun list-parse-number (x)
  "recursively turn nested lists of strings into nested lists of numbers while preserving the list structure"
  (cond
    ((null x) x)
    ((listp x) (cons
                (list-parse-number (car x))
                (list-parse-number (cdr x))))
    ((and (stringp x) (not-empty-strin-p x)) (parse-number x))
    (t '())))

(defun remove-nil (lst)
  "remove all nil values from a nested list, may have to be run twice to remove all nils"
  (cond
    ((eq (length lst) 0) nil)
    ((null (car lst)) (remove-nil (cdr lst)))
    ((listp (car lst)) (cons (remove-nil (car lst)) (remove-nil (cdr lst))))
    (t (cons (car lst) (remove-nil (cdr lst))))))

(defun parse-file (path &optional (separator #\,))
  "read in a csv file and return a nested list of only numbers with headers removed"
  (remove-nil (remove-nil (list-parse-number (remove-headers (read-file (pathname path) separator))))))

(defun column (n data)
  "given a table in form of nested lists, return a list containing all entries of one column"
   (cond ((null data) '())
         (t (cons
             (nth n (car data))
             (column n (cdr data))))))
