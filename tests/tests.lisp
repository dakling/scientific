(in-package :scientific)

(assert (= (sqrt (+ 25 49 81))
           (magnitude (+ (list 1 2 3)
                            (list->mvector 4 5 6)))
           (magnitude (+ (list->mvector 4 5 6)
                            (list 1 2 3)))
           (magnitude (+ (list->mvector 1 2 3)
                            (list->mvector 4 5 6)))
           (magnitude (+ (list->mvector 4 5 6)
                            (list->mvector 1 2 3)))
           (magnitude (+ (list 1 2 3)
                            (list 4 5 6)))
           (magnitude (+ (list 4 5 6)
                            (list 1 2 3)))))
(assert (= (sqrt (+ 9 9 9))
           (magnitude (- (list 1 2 3)
                            (list->mvector 4 5 6)))
           (magnitude (- (list->mvector 1 2 3)
                            (list->mvector 4 5 6)))
           (magnitude (- (list 1 2 3)
                            (list 4 5 6)))))

(assert (= 32
           (* (list 1 2 3)
               (list->mvector 4 5 6))
           (* (list->mvector 4 5 6)
               (list 1 2 3))
           (* (list->mvector 1 2 3)
               (list->mvector 4 5 6))
           (* (list->mvector 4 5 6)
               (list->mvector 1 2 3))
           (* (list 1 2 3)
               (list 4 5 6))
           (* (list 4 5 6)
               (list 1 2 3))))

;; TODO
;; (assert (= (sqrt (+ (expt 1/4 2) (expt 5/2 2) (expt 2 2)))
;;            (magnitude (/ (list 1 2 3)
;;                             (list->mvector 4 5 6)))
;;            (magnitude (/ (list->mvector 1 2 3)
;;                             (list->mvector 4 5 6)))
;;            (magnitude (/ (list 1 2 3)
;;                             (list 4 5 6)))))

(defun almost-equal-p (&rest args)
  (reduce (lambda (x y) (< (abs (- x y)) 1d-5))
          args))

(assert
 (almost-equal-p
  (sqrt 2)
  (first
   (newton
    (lambda (x)
      (- (expt x 2) 2))
    1d0
    :tolerance 1d-6))))

(assert
 (almost-equal-p
  (sqrt 2)
  (first
   (newton
    (lambda (x)
      (- (expt x 2) 2))
    (ones 1)
    :tolerance 1d-6))))

;; TODO
;; (assert
;;  (reduce
;;   (map
;;    #'almost-equal-p
;;    (list (sqrt 2) (sqrt 3))
;;    (newton
;;     (lambda (x y)
;;       (list
;;        (- (expt x 2) 2)
;;        (- (expt y 2) 3)))
;;     (ones 2)))))
