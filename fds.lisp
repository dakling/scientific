(in-package :scientific)

(defun diff-1d (f &optional x (delta 1d-6))
  (let
      ((df
         (lambda (xvalue)
           (/
            (-
             (funcall f (+ xvalue delta))
             (funcall f (- xvalue delta)))
            (* 2 delta)))))
    (if x
        (funcall df x)
        df)))

(defun diff (f x &optional (component 0) (delta 1d-6))
  (let ((len (length x)))
    (/
     (- (gapply f (+ x (unit-list-scaled component len delta)))
        (gapply f (- x (unit-list-scaled component len delta))))
     (* 2 delta))))

(defun jacobian-fds (func x &optional (delta 1d-6))
  ;; (if (not (= (length (gapply func x)) (length x)))
  ;;     (error "test")
  ;;     (error "test"))
  (labels
      ((df (i j)
         (gref (diff func x j delta) i)))
    (gen-matrix (length (gapply func x)) (length x) #'df)))

;; (diff
;;  (lambda (x y) (list (* y x) (expt y 2)))
;;  (list 2d0 4d0)
;;  1)

;; (jacobian-fds (lambda (x) (list (* x) (expt x 2)))
;;               (list 2d0))
