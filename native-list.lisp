(in-package :scientific)

(defmethod add ((arg1 list) (arg2 list))
  (map #'+ arg1 arg2))

(defmethod negate ((arg1 list))
  (* -1 arg1))

(defmethod subtract ((arg1 list) (arg2 list))
  (+ arg1 (negate arg2)))


;; TODO dubious but useful
(defmethod add ((arg1 list) (arg2 number))
  (map (lambda (x) (+ x arg2))
          arg1))

(defmethod subtract ((arg1 list) (arg2 number))
  (+ arg1 (negate arg2)))

(defmethod add ((arg2 number) (arg1 list))
  (+ arg1 arg2))

(defmethod subtract ((arg1 number) (arg2 list))
  (+ arg1 (negate arg2)))

(defmethod multiply ((arg1 list) (arg2 list))
  (assert (compatible-p arg1 arg2) nil "Input lists are not compatible for performing the dot product.")
  (reduce #'+ (map #'* arg1 arg2)))

;; (* (list 1 2 3) (list 1 2 3))

(defmethod multiply ((arg1 list) (arg2 number))
  (map (lambda (x) (* x arg2))
       arg1))

(defmethod multiply ((arg1 number) (arg2 list))
  (* arg2 arg1))

(defmethod divide ((arg1 list) (arg2 list))
  (map (lambda (x y) (/ x y)) arg1 arg2))

(defmethod divide ((arg1 list) (arg2 number))
  (map (lambda (x) (/ x arg2)) arg1 ))

;; (map (lambda (x y z a) (+ x y)) (list 1 2 3) (list 3 4 5) (list 3 4 5) (list 3 4 5))

(defmethod expt ((arg1 list) (arg2 number))
  (map (lambda (x) (expt x arg2))
        arg1))

(defmethod expt ((arg1 number) (arg2 list))
  (map (lambda (x) (expt arg1 x))
        arg2))

(defmethod expt ((arg1 list) (arg2 list))
  (cond
    ((= 1 (length arg1)) (expt (first arg1) arg2))
    ((= 1 (length arg2)) (expt arg1 (first arg2)))
    ((= (length arg1) (length arg2)) (map #'expt arg1 arg2))))

;; TODO nested lists
(defmethod gref ((arg1 list) n &rest other-ns)
  (if other-ns
      (apply #'gref (gref arg1 n) other-ns)
      (if (> 0 n)
          (elt arg1 (+ (length arg1) n))
          (elt arg1 n))))

(defun unit-list-scaled (index length &optional (factor 1d0))
  (loop for i from 0 below length
        collect (if (= i index)
                    factor
                    0d0)))

(defmethod magnitude ((arg1 list))
  (sqrt (* arg1 arg1)))

;; TODO check if really needed
(defmethod gapply (func (arg list) &rest other-args)
  (if other-args
      (if (cdr other-args)
          (apply func arg other-args)
          (apply func arg (car other-args)))
      (apply func arg)))

;; (apply (lambda (lst1 lst2) (first lst1))
;;        (list->vector 1 2 3)
;;        (list 4))

;; (gref (list (list 1 2)
;;             (list 3 4))
;;       1 0)

;; (gref (list (list 1 2)
;;             (list 3 4))
;;        1)

;; (let ((arg1
;;         (list (list 1 2)
;;               (list 3 4))))
;;   (gapply #'gref (gref arg1 1) (list 0))
;;   ;; (apply #'gref (gref arg1 1) (list 0))
;;   ;; (gapply #'gref arg1 1 0)
;;   )

(defmethod gcons (arg1 (arg2 list))
  (cons arg1 arg2))

;; TODO think about if this makes sense
(defmethod gcons ((arg1 list) (arg2 list))
  (append arg1 arg2))

;; TODO think about which is better - with or without list
(defmethod gcons (arg1 (arg2 number))
  (cons arg1 (list arg2)))

(defmethod compatible-p ((arg1 list) (arg2 list))
  (=
   (length arg1)
   (length arg2)))

(defmethod rectangular-p ((arg list))
  (gapply #'=
          (map #'length arg)))

(defmethod g-transpose ((arg1 list))
  (assert (rectangular-p arg1) nil "Cannot transpose non-rectangular list!")
  (loop for i from 0 below (length (gref arg1 0))
        collecting
        (loop for j from 0 below (length arg1)
              collecting (gref arg1 j i))))

(defmethod gsplit ((lst list) parts)
  (assert (= 0 (mod (length lst) parts)) nil "unable to split ~a into ~a equal pieces" lst parts)
  (let ((part-length (/ (length lst) parts)))
    (map
     (lambda (i)
       (subseq lst (* i part-length) (* (1+ i) part-length)))
     (gbutlast (a:iota (1+ parts))))))

(defmethod g-flatten ((lst list))
  (a:flatten
   (loop for elem in lst
         collecting (cond
                      ((m::vector-p elem) (mvector->list elem))
                      ((row-matrix-p elem) (vector->row-matrix elem))
                      ((column-matrix-p elem) (vector->column-matrix elem))
                      ((listp elem) elem)
                      (t (error "Cannot flatten list containing the element ~A" elem))))))

(defmethod g-to-list ((arg list))
  arg)

(defmethod g-to-vector ((arg list))
  (apply #'vector arg))

;; (g-transpose (list
;;               (list->vector 1 2 3)
;;               (list->vector 3 4 5)))

;; (gref (list
;;        (list 1 2 3)
;;        (list 3 4 5))
;;       0 0)

;; (let ((arg1
;;         (list
;;          (list->vector 1 2 3)
;;          (list->vector 3 4 5))))
;;   ;; (gref (gref arg1 0) 2)
;;   ;; (apply #'gref (list->vector 1 2 3) (list 0))
;;   (gapply #'gref (list->vector 1 2 3) (list 0)))

;; ;; (rectangular-p (list
;; ;;               (list 1 2 3)
;; ;;               (list 3 4 5)))
