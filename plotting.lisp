(in-package :scientific)

;; (asdf:load-system :py4cl)

;; (let ((py4cl:*python-command* "python3"))
;;  (py4cl:import-module "matplotlib.pyplot" :as "plt" :reload t))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (py4cl:import-module "matplotlib.pyplot" :as "plt" :reload t))

(defun plot-gnuplot (x-data y-data &key
                                     (x-label "$x$") (y-label "$y$")
                                     x-lim y-lim
                                     x-log? y-log?
                                     (format "") (title "") (legend ""))
  "plot given data using gnuplot"
  (vgplot:xlabel x-label)
  (vgplot:ylabel y-label)
  (when x-log? (vgplot:semilogx))
  (when y-log? (vgplot:semilogy))
  (vgplot:title title)
  (vgplot:axis (g-flatten (list x-lim y-lim)))
  (vgplot:plot (g-to-list x-data) (g-to-list y-data) (format nil "~A;~A;" format legend)))

(defun plot-matplotlib (x-data y-data &key
                                        (x-label "$x$") (y-label "$y$")
                                        x-lim y-lim
                                        x-log? y-log?
                                        (format "") (title "") (legend ""))
  "plot given data using matplotlib"
  (plt:plot (g-to-list x-data) (g-to-list y-data) format :label legend)
  (plt:xlabel x-label)
  (plt:ylabel y-label)
  (when x-lim (plt:xlim x-lim))
  (when y-lim (plt:ylim y-lim))
  (when x-log? (plt:semilogx))
  (when y-log? (plt:semilogy))
  (plt:title title)
  (plt:legend)
  (plt:grid t))

(defun save-plot (filename plotting-backend)
  (cond
    ((equalp plotting-backend 'gnuplot) (vgplot:print-plot (pathname filename)))
    ((equalp plotting-backend 'matplotlib) (plt:savefig filename))))

(defun clear-plot (plotting-backend)
  (cond
    ((equalp plotting-backend 'gnuplot) nil)
    ((equalp plotting-backend 'matplotlib) (plt:clf))))

(defun plot (x-data y-data
             ;; &rest other-data-sets
             &key
               (x-label "$x$") (y-label "$y$")
               x-lim y-lim
               x-log? y-log?
               (format "") (title "") (legend "")
               filename
               (plotting-backend 'matplotlib))
  ;; (clear-plot plotting-backend)
  (funcall
   (cond
     ((equalp plotting-backend 'gnuplot) #'plot-gnuplot)
     ((equalp plotting-backend 'matplotlib) #'plot-matplotlib)
     (t (error "Unknown plotting backend: ~A" plotting-backend)))
   x-data y-data
   :x-label x-label
   :y-label y-label
   :x-lim x-lim
   :y-lim y-lim
   :x-log? x-log?
   :y-log? y-log?
   :format format
   :title title
   :legend legend
   ;; other-data-sets
   )
  (when filename (save-plot filename 'plotting-backend)))

(defun plot-ode-solution (sol &key
                                (x-label "$x$") (y-label "$y$")
                                x-lim y-lim
                                (legend "")
                                (format "")
                                x-log? y-log?
                                reference-solutions
                                reference-solution-formats
                                (index-list (list 0))
                                (filename "plot.pdf")
                                (plotting-backend 'matplotlib)
                                multiplot?)
  (unless multiplot? (clear-plot plotting-backend))
  (map
   (lambda (index) (plot (ode-solution-grid sol) (ode-solution->list sol index)
                :x-label x-label
                :y-label y-label
                :x-lim x-lim
                :y-lim y-lim
                :x-log? x-log?
                :y-log? y-log?
                ;; :format format
                :legend (or (if (and (ode-solution-name sol) (listp (ode-solution-name sol)))
                                (gref (ode-solution-name sol) index) (ode-solution-name sol))
                            legend)
                ;; :filename filename
                :plotting-backend plotting-backend))
   (g-to-list index-list))
  (when reference-solutions
    (map
     (lambda (reference-solution fmt)
       (plot
           (ode-solution-grid reference-solution)
         (ode-solution-solution reference-solution)
         ;;   (ode-solution-grid sol)
         ;; (map reference-solution
         ;;      (ode-solution-grid sol))
         :legend (ode-solution-name reference-solution)
         :format fmt
         :x-label x-label
         :y-label y-label
         :x-lim x-lim
         :y-lim y-lim
         :x-log? x-log?
         :y-log? y-log?
         :plotting-backend plotting-backend))
     (g-to-list reference-solutions)
     (or
      reference-solution-formats
      (make-list (length reference-solutions) :initial-element ""))))
  (unless multiplot? (save-plot filename plotting-backend)))

(defun plot-grid (grid &key log? (filename "grid.pdf"))
  (clear-plot 'matplotlib)
  (plot-matplotlib
   grid
   (map (lambda (x) (declare (ignore x)) 0d0) grid)
   :format "b."
   :x-log? log?)
  (save-plot filename 'matplotlib))

;; (plot-gnuplot (list 0 1 2 3) (list 0 1 2 3))
;; (plot-gnuplot (list 0 1 2 3) (list 0 1 4 9))
;; (save-plot "test.pdf" 'gnuplot)
