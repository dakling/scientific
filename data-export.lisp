(in-package :scientific)

(defun write-data-to-file (filename x-data &rest y-data)
  (cl-csv:write-csv (g-transpose (apply #'list x-data y-data)) :stream (pathname filename)))

;; (write-data-to-file #P"out.txt" (list 1 2 3) (list 1 4 9) (list 1 8 27))
