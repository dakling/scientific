(in-package :scientific)

(defun l2-norm (lst)
  (magnitude lst))

(defun reductions (function sequence &rest args)
  "Return a list of intermediate values from reducing SEQUENCE with FUNCTION."
  (let* ((reductions (list))
         (result (apply #'reduce
                        (lambda (&rest arguments)
                          (let ((result (apply function arguments)))
                            (push result reductions)
                            result))
                        sequence
                        args)))
    (values (or (nreverse reductions)
                (list result))
            result)))

(defun zeros (&rest len)
  (m:zeros len))

(defun ones (&rest len)
  (m:ones len))

(defun range (start end steps)
  (list->mvector
   (loop for i from 0 below steps
         collecting (+ start
                       (* 1d0 (- end start) i (/ (1- steps)))))))

(defun alist->hash-table (alist)
  (let ((out (make-hash-table)))
    (loop for elem in alist
          do (setf (gethash (car elem) out) (cdr elem)))
    out))

(defun strip-list-before-element (lst element)
  (if element
      (remove-if-not (lambda (x) (<= element x)) lst)
      lst))

(defun strip-list-until-element (lst element)
  (if element
      (remove-if-not (lambda (x) (>= element x)) lst)
      lst))
