(in-package :scientific)

(defun integrate (x y &key start end)
  (if (and start end (> start end))
      (- (integrate x y :start end :end start))
      (let*
          ((xs-without-end (strip-list-until-element
                            (g-to-list x)
                            end))
           (ys-without-end (subseq y 0 (length xs-without-end)))
           (xs (strip-list-before-element
                xs-without-end
                start))
           (ys (subseq ys-without-end (- (length xs-without-end) (length xs))))
           (y-n+1 (grest ys))
           (y-n (gbutlast ys))
           (x-n+1 (grest xs))
           (x-n (gbutlast xs)))
        (/
         (* (+ y-n+1 y-n)
            (- x-n+1 x-n))
         2))))

;; (integrate (list 0 1 2 3 4 5) (list 0 1 4 9 16 25) :start 2 :end 4)
;; (integrate (list 0 1 2 3 4 5) (list 0 1 4 9 16 25) :start 4 :end 2)
;; (integrate (list 0 1 2 3) (list 0 1 4 9))

(defun integrate-fn (fn start &optional (intervals 100))
  (lambda (x)
    (let ((xs (range start x intervals)))
      (integrate xs (map fn xs)))))


(defun make-nonuniform-grid (start end number-of-cells func)
  (let* ((mapped-grid (map (integrate-fn func 0) (range 0 1 number-of-cells)))
         (mapped-start (gref mapped-grid 0))
         (mapped-end (gref mapped-grid -1)))
   (map (lambda (x) (+ start
                       (* x
                          (/ (- end start)
                             (- mapped-end
                                mapped-start)))))
        mapped-grid)))

(defun make-uniform-grid (start end number-of-cells)
  (make-nonuniform-grid start end number-of-cells (lambda (x) 1d0)))

(defun make-exponential-grid-left (start end number-of-cells)
  (make-nonuniform-grid start end number-of-cells (lambda (x) (exp x))))

(defun make-exponential-grid-right (start end number-of-cells)
  (make-nonuniform-grid start end number-of-cells (lambda (x) (exp (- 1 x)))))

(defun make-exponential-grid-both (start end number-of-cells)
  (make-nonuniform-grid start end number-of-cells (lambda (x) (exp (- (/ (expt (- x 0.5) 2) 0.5))))))

(defun grid-number-of-nodes (grid)
  (length grid))

(defstruct ode-solution
  grid
  solution
  name)

(defun table->ode-solution (table &optional (name "data"))
  (let ((grid (map #'first table))
        (solution (map #'second table)))
    (make-ode-solution :grid grid
                       :solution solution
                       :name name)))

(defun ivp-output->ode-solution (ivp-output)
  (let ((grid (map #'first ivp-output))
        (solution (g-transpose
                   (map (lambda (x) (first (grest x)))
                        ivp-output))))
    (make-ode-solution :grid grid
                       :solution solution)))

(defun bvp-output->ode-solution (grid bvp-output number-of-vars &optional (names "numerical solution"))
  (let ((solution (gsplit (g-to-list bvp-output) number-of-vars)))
    (make-ode-solution :grid grid
                       :solution solution
                       :name names)))

(defun ode-solution-number-of-dependent-variables (ode-solution)
  (length (ode-solution-solution ode-solution)))

(defun ode-solution->list (sol &optional (index 0))
  (gref (ode-solution-solution sol) index))

;; TODO
;; (defun ode-solution-timestep->list (sol &optional (index 0))
;;   (map (lambda ()) (ode-solution-solution sol)))

(defun ode-solution->func (sol &optional (index 0))
  (data->func (g-to-list (ode-solution-grid sol)) (g-to-list (ode-solution->list sol index))))

(defun ode-solution->bvp-initial-guess (sol)
  (ode-solution-solution sol))

;; TODO put names in header
(defun write-ode-solution-to-file (sol &optional (filename #p"ode-solution.csv"))
  (apply
   #'write-data-to-file
   filename
   (ode-solution-grid sol)
   (g-to-list (ode-solution-solution sol))))

;; TODO read names from header
(defun read-ode-solution-from-file (filename &optional names)
  (let*
      ((raw-data (parse-file filename))
       (grid (map #'first raw-data))
       (solution (g-transpose (map #'rest raw-data))))
    (make-ode-solution
     :grid grid
     :solution solution
     :name names)))

(defun map-ode-solution-to-grid (grid ode-solution)
  (make-ode-solution
   :grid grid
   :solution (map
              (lambda (index)
                (map
                 (ode-solution->func ode-solution index)
                 grid))
              (a:iota (ode-solution-number-of-dependent-variables ode-solution)))
   :name (ode-solution-name ode-solution)))

;; (map-ode-solution-to-grid
;;  (make-uniform-grid 0 1 10)
;;  (make-ode-solution
;;   :grid (make-uniform-grid 0 1 5)
;;   :solution
;;   (list
;;    (ones 5)
;;    (range 4 6 5)
;;    (range 0 1 5))))

(defun explicit-euler-step (func current-sol old-time new-time &key (max-iter 5000) (tolerance 1d-10) (write-interval 10))
  (declare (ignore max-iter tolerance write-interval))
  (let ((delta-time (- new-time old-time)))
    (values
     (list
      new-time
      (+
          current-sol
          (* delta-time
             (funcall func old-time current-sol))))
     0d0)))

(defun implicit-euler-step (func current-sol old-time new-time &key (max-iter 5000) (tolerance 1d-10) (write-interval 10))
  (let ((delta-time (- new-time old-time)))
    (multiple-value-bind (solution residual)
        (hybrid-scaled
         ;; broyden
         ;; fletcher-reeves
         (lambda (&rest y-new)
           (+
            y-new
            (-
             (* delta-time
                (funcall func new-time y-new)))
            (- current-sol)))
         current-sol
         :write-interval write-interval
         :max-iter max-iter
         :tolerance tolerance)
      (progn
        (if (> residual 1d-10) (error "failed to converge"))
        (values
         (list
          new-time
          solution)
         residual)))))

(defun crank-nicholson-step (func current-sol old-time new-time &key (max-iter 5000) (tolerance 1d-10) (write-interval 10))
  (let ((delta-time (- new-time old-time)))
    (multiple-value-bind (solution residual)
        (hybrid-scaled
         ;; broyden
         ;; fletcher-reeves
         ;; newton
         ;; nelder-mead
         ;; scipy-root
         (lambda (&rest y-new)
           (+
            y-new
            (-
             (* delta-time 1/2
                (+
                 (funcall func new-time y-new)
                 (funcall func old-time current-sol))))
            (- current-sol)))
         current-sol
         :write-interval write-interval
         :max-iter max-iter
         :tolerance tolerance)
      (values
       (list
        new-time
        solution)
       residual))))

;; slight HACK
(defvar *last-time*)

(defun timestepper (start-time end-time number-of-steps func initial-cond &key (timestepping-method #'implicit-euler-step) (max-iter 5000) (tolerance 1d-10))
  (ivp-output->ode-solution
   (reductions
    (lambda (current-time-and-sol new-time)
      (destructuring-bind
          (current-time current-sol)
          current-time-and-sol
        (format t "time: ~A~&" current-time)
        (setf *last-time* current-time)
        (funcall timestepping-method func current-sol current-time new-time :max-iter max-iter :tolerance tolerance)))
    (rest (g-to-list (range start-time end-time number-of-steps)))
    :initial-value (list start-time initial-cond))))

(defun adaptive-pseudo-timestepper (start-time end-time number-of-steps func initial-cond &key (timestepping-method #'implicit-euler-step) (max-iter 5000) (tolerance 1d-15))
  (labels
      ((iter (time current-sol delta-t acc good-count bad-count)
         (let*
             ((new-time (+ time delta-t))
              (new-delta-good (* 1.9 delta-t))
              (new-delta-bad (* 0.5 delta-t)))
           (multiple-value-bind (new-time-and-sol residual)
               (funcall timestepping-method func current-sol time new-time :max-iter max-iter :tolerance tolerance :write-interval nil)
               ;; (or
               ;;  (ignore-errors
               ;;   (funcall timestepping-method func current-sol time new-time :max-iter max-iter :tolerance tolerance))
               ;;  (values current-sol (+ 1 tolerance)))
             (let ((new-sol (cadr new-time-and-sol)))
              (format t "current time: ~A~&" time)
              (format t "current residual: ~A~&" residual)
              (format t "good steps: ~A; bad steps:~A~&" good-count bad-count)
              (cond
                ((< new-delta-bad 1d-100) (error "Failed to obtain a solution by decreasing the timestep!"))
                ((not residual)
                 (progn
                   (iter new-time new-sol delta-t
                     (cons (list new-time new-sol) acc)
                     good-count bad-count)))
                ((and (> tolerance residual) (< end-time time) (or t (< (magnitude (- current-sol new-sol)) tolerance)))
                 (progn
                   (format t "converged successfully!")
                   (reverse acc)))
                ((and residual (< residual tolerance))
                 (progn
                   (format t "increasing timestep size to ~A~&" new-delta-good)
                   (iter new-time new-sol new-delta-good
                     (cons (list new-time new-sol) acc)
                     (1+ good-count) bad-count)))
                (t
                 (progn
                   (format t "decreasing timestep size to ~A~&" new-delta-bad)
                   (iter time current-sol new-delta-bad acc good-count (1+ bad-count))))))))))
    (let ((sol (iter start-time initial-cond
                 (/ (- end-time start-time) number-of-steps)
                 (list (list start-time initial-cond))
                 0 0)))
      (ivp-output->ode-solution sol)
      ;; (make-ode-solution
      ;;  :grid (car sol)
      ;;  :solution (cadr sol))
      )))

(defun fdm-discretizer-1st-derivative (x-left x-right y-left y-right)
  "Approximate \partial fun(x,y) / \partial x in a cell from x-left to x-right."
  (/
   (- y-right y-left)
   (- x-right x-left)))

(defun fdm-discretizer-1st-derivative-3 (x-left x-middle x-right y-left y-middle y-right)
  "Approximate \partial fun(x,y) / \partial x in a cell from x-left to x-right."
  (/
   (+
    (/
     (- y-right y-middle)
     (- x-right x-middle))
    (/
     (- y-left y-middle)
     (- x-left x-middle)))
   2)
  ;; (/
  ;;  (-
  ;;   y-right
  ;;   y-left)
  ;;  (- x-right x-left))
  )

(defun fdm-discretizer-2nd-derivative (x-left x-middle x-right y-left y-middle y-right)
  "Approximate \partial fun(x,y) / \partial x in a cell from x-left to x-right."
  (let
      ((x-left-sq (expt x-left 2))
       (x-middle-sq (expt x-middle 2))
       (x-right-sq (expt x-right 2)))
    (*
     2
     (/
      (-
       (*
        (- y-middle y-left)
        (- x-right x-left))
       (*
        (- y-right y-left)
        (- x-middle x-left)))
      (-
       (*
        (- x-middle-sq x-left-sq)
        (- x-right x-left))
       (*
        (- x-right-sq x-left-sq)
        (- x-middle x-left))))))
  ;; (/
  ;;  (+
  ;;   y-right
  ;;   (* -2d0 y-middle)
  ;;   y-left)
  ;;  (* (- x-right x-middle) (- x-middle x-left)))
  )

(defun solve-bvp-system (left right number-of-nodes bvp-func boundary-conditions initial-guess &key (nonlinear-solver #'hybrid-scaled) (max-iter 10000) (tolerance 1d-10) (grid-function #'make-uniform-grid))
  (let ((grid (funcall grid-function left right number-of-nodes))
        (number-of-vars (length initial-guess)))
    (labels
        ((discretized-system-residual (y-solution)
           (g-flatten
            (map
             (lambda (x-middle x-right y-middle y-right x-index)
               (- (fdm-discretizer-1st-derivative
                   x-middle x-right y-middle y-right)
                   (funcall bvp-func x-middle x-index y-middle grid y-solution)))
             ;; (mvector->list (gdrop-left grid 2))
             (mvector->list (gdrop-right grid 1))
             (mvector->list (gdrop-left grid 1))
             ;; (mvector->list (gdrop-left (g-transpose y-solution) 2))
             (mvector->list (gdrop-right (g-transpose y-solution) 1))
             (mvector->list (gdrop-left (g-transpose y-solution) 1))
             (a:iota number-of-nodes))))
         (boundary-condition-residual (y-solution)
           (funcall boundary-conditions grid y-solution)))
      (multiple-value-bind (raw-result residual-per-dof)
              (funcall nonlinear-solver
                       (lambda (&rest full-solution-vector)
                         (let ((y-solution (gsplit (g-to-list full-solution-vector) number-of-vars)))
                           (gcons
                            (funcall #'boundary-condition-residual y-solution)
                            (funcall #'discretized-system-residual y-solution))))
                       (g-flatten initial-guess)
                       :tolerance tolerance
                       :max-iter max-iter)
        (progn
         (values
          (bvp-output->ode-solution
           grid
           raw-result
           number-of-vars)
          (< residual-per-dof tolerance)))))))

;; (let ((number-of-nodes 100))
;;   (plot-ode-solution
;;    (solve-bvp-system
;;     0d0 5d0 number-of-nodes
;;     (lambda (x y)
;;       (list
;;        (gref y 1)
;;        (- (gref y 0))))
;;     (lambda (x y-vec)
;;       (list
;;        (gref (gref y-vec 0) 0)                  ; y(0)=0
;;        ;; (- (gref (gref y-vec 1) 0) 1d0) ; y'(0)=1
;;        (- (gref (gref y-vec 0) -1) (sin 5d0))
;;        ))                               ; y'(pi)=1
;;     (list
;;      (zeros number-of-nodes)
;;      (zeros number-of-nodes)
;;      ;; (map #'sin (range 0d0 5d0 number-of-nodes))
;;      ;; (map #'cos (range 0d0 5d0 number-of-nodes))
;;      )
;;     ;; :nonlinear-solver #'nelder-mead
;;     :nonlinear-solver #'hybrid-scaled)
;;    :index-list (list 0)
;;    :analytical-solution #'sin))
;; (let ((number-of-nodes 50))
;;   (plot-ode-solution
;;    (solve-bvp 0d0 5d0 number-of-nodes
;;               (lambda (x y)
;;                 (list
;;                  (+ (gdiff y 2) y)))   ;TODO
;;               (lambda (x y-vec)
;;                 (list
;;                  (gref (gref y-vec 0) 0)                   ; y(0)=0
;;                  (- (gref (gref y-vec 0) -1) (sin 5d0)))) ; y'(pi)=1
;;               (list
;;                (zeros number-of-nodes)
;;                ;; (map #'sin (range 0d0 5d0 number-of-nodes))
;;                )
;;               ;; :nonlinear-solver #'nelder-mead
;;               :nonlinear-solver #'hybrid-scaled)
;;    :analytical-solution #'sin))

;; (let ((steps 100))
;;   (integrate
;;    (range 0 1 steps)
;;    (map (lambda (x) (* x x)) (range 0 1 steps))))

;; solution-format:
;; (map #'grest
;;  (list
;;   (zeros 5)                             ;first variable, discretized on x
;;   (ones 5)                              ;second variable, discretized on x
;;   ))

;; (funcall
;;  (lambda (x y)
;;    (list
;;     (- (gref y 0) (exp x))
;;     (+ (gref y 1) (exp x))))
;;  0 (list 1 3))

;; (defun simple-ode (x u)
;;   (list
;;    (gref u 1)
;;    (- (gref u 0))))

;; stepping backwards -> seems to work
;; (let*
;;     ((sol (adaptive-timestepper 0d0 5d0 300 #'simple-ode
;;                        (list
;;                         0d0
;;                         -1d0)
;;                        :timestepping-method
;;                        ;; #'implicit-euler-step
;;                        ;; #'explicit-euler-step
;;                        #'crank-nicholson-step
;;                        :max-iter 100
;;                        :tolerance 1d-5
;;                        ))
;;      (err (-
;;            (map (lambda (x) (sin x))
;;                  (ode-solution->list sol 0)) ;analytical solution
;;            (ode-solution->list sol)))
;;      )
;;   (/ (magnitude err) (length err))
;;   (plot-ode-solution sol
;;                      :plotting-backend 'matplotlib)
;;   ;; (- (exp 1) (last (ode-solution->list sol)))
;;   )

;; (crank-nicholson-step #'simple-ode (list 0d0 -1d0) 0 0.1)


(py4cl:import-module "scipy.integrate" :as "pyint" :reload t)
(defvar *u* nil)
(defun py-solve-bvp-system (left right number-of-nodes bvp-func boundary-conditions initial-guess &key &allow-other-keys)
    (let*
        ((grid (make-array number-of-nodes :initial-contents (g-to-list (range left right number-of-nodes))))
         (res
           (py4cl:remote-objects
             (pyint:solve_bvp
              (lambda (x y)
                (setf *u*
                      (g-transpose
                       (map (lambda (index) (funcall bvp-func (gref x index)
                                                (map (lambda (var-index) (gref y var-index index))
                                                     (alexandria:iota (array-dimension y 0)))))
                            (alexandria:iota (array-dimension y 1))))))
              (funcall boundary-conditions grid (or *u* initial-guess))
              ;; (lambda (ya yb)
              ;;   (funcall boundary-conditions
              ;;            (list ya TODO yb)))
              grid
              initial-guess))))
      (format t "~A" (py4cl:python-method res "get" "message"))
      (py4cl:python-method res "get" "y")))

;; (let*
;;     ((number-of-nodes 5)
;;      (res
;;        (py4cl:remote-objects
;;          (pyint:solve_bvp
;;           (lambda (x y)
;;             (let* ((dim (array-dimensions y))
;;                    (no-nods (gref dim 1)))
;;               (make-array
;;                dim
;;                :initial-contents (list
;;                                   (map
;;                                    (lambda (i)
;;                                      (aref y 1 i))
;;                                    (a:iota no-nods))
;;                                   (map
;;                                    (lambda (i)
;;                                      (- (aref y 0 i)))
;;                                    (a:iota no-nods))))))
;;           (lambda (ya yb)
;;             (make-array (list 2) :initial-contents (list (gref ya 0) (1- (gref yb 0)))))
;;           (make-array number-of-nodes :initial-contents (g-to-list (range 0 (/ pi 2) number-of-nodes)))
;;           (make-array (list 2 number-of-nodes) :initial-element 0)
;;           ))))
;;   (py4cl:python-method res "get" "message"))
