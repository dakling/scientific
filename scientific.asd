;;;; scientific.asd

(asdf:defsystem #:scientific
  :description "Describe scientific here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:generic-cl
               #:py4cl
               #:vgplot
               #:magicl
               #:alexandria
               #:serapeum
               #:cl-csv
               #:cl-strings
               #:mgl
               #:gsll)
  :components ((:file "package")
               (:file "generics")
               (:file "helpers")
               (:file "native-list")
               (:file "native-vectors")
               (:file "linear-algebra")
               (:file "mgl")
               (:file "arrays")
               (:file "functions")
               (:file "data-import")
               (:file "data-export")
               (:file "interpolation")
               (:file "fds")
               (:file "nlopt")
               (:file "ode")
               (:file "plotting")
               (:file "scientific")))

(asdf:defsystem #:scientific/test
  :depends-on (:scientific
               :prove)
  :defsystem-depends-on (:prove-asdf)
  :components
  ((:test-file "tests/tests"))
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run) :prove) c)))
