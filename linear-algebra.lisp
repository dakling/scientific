(in-package :scientific)

(defmethod elt ((arg1 m::abstract-tensor) index)
  (if (listp index)
      (apply #'m:tref arg1 index)
      (m:tref arg1 index)))

(defmethod first ((arg1 m::matrix))
  (elt arg1 '(0 0)))

(defun gen-matrix (rows cols func)
  "Generate a matrix with shape (rows cols), where the elements are obtained by evaluating func."
  (let ((out (m:zeros (list rows cols))))
    (loop for i from 0 below rows
          do (loop for j from 0 below cols
                   do (setf (m:tref out i j) (* 1d0 (funcall func i j)))))
    out))

(defun gen-mvector (length func)
  "Generate a matrix with shape (rows cols), where the elements are obtained by evaluating func."
  (let ((out (m:zeros (list length))))
    (loop for i from 0 below length
          do (setf (m:tref out i) (* 1d0 (funcall func i))))
    out))

(defun mvector->list (vec)
  (if (listp vec)
      vec
      (loop for i from 0 below (m:size vec)
            collecting (m:tref vec i))))

(defun list->mvector (&rest lst)
  (if (and (= 1 (length lst)) (listp (car lst)))
      (apply #'list->mvector (car lst))
      (m:from-list lst (list (length lst)))))

(defun mvector->vector (vec)
  (if (vectorp vec)
      vec
      (g-to-vector
       (loop for i from 0 below (m:size vec)
             collecting (m:tref vec i)))))

(defun vector->mvector (vec)
  (let ((len (length vec)))
      (let ((out (m:zeros (list len))))
        (loop for i from 0 below len
              do (setf (m:tref out i) (* 1d0 (elt vec i))))
        out)))

(defun column-matrix-p (mat)
  (and
   (m::matrix-p mat)
   (= 1 (gref (m:shape mat) 1))))

(defun row-matrix-p (mat)
  (and
   (m::matrix-p mat)
   (= 1 (gref (m:shape mat) 0))))

(defun mvector->column-matrix (vec)
  (let ((len (m:size vec)))
    (gen-matrix len 1
                (lambda (i j)
                  (m:tref vec i)))))

(defun mvector->row-matrix (vec)
  (m:transpose (mvector->column-matrix vec)))

(defmethod compatible-p ((vec m::vector) (mat m::matrix))
  (and
   (or (row-matrix-p mat)
       (column-matrix-p mat))
   (= (length vec)
      (length mat))))

(defmethod compatible-p ((mat m::matrix) (vec m::vector))
  (compatible-p vec mat))

(defun mvector->compatible-matrix (vec mat)
  (cond
    ((column-matrix-p mat) (mvector->column-matrix vec))
    ((row-matrix-p mat) (mvector->row-matrix vec))
    (t (error "Unable to turn mvector into compatible matrix."))))

(defun column-matrix->mvector (mat)
  (gen-mvector (length mat)
              (lambda (i)
                (gref mat i))))

(defun row-matrix->mvector (mat)
  (gen-mvector (length mat)
              (lambda (i)
                (gref mat i))))

(defun matrix->mvector (mat)
  (cond
    ((column-matrix-p mat) (column-matrix->mvector mat))
    ((row-matrix-p mat) (row-matrix->mvector mat))
    ((m::vector-p mat) mat)
    ((listp mat) (list->mvector mat))
    (t (error "Unable to turn matrix into mvector."))))

(defun matrix->list (mat)
  (mvector->list (matrix->mvector mat)))

(defun list->row-matrix (&rest lst)
  (mvector->row-matrix (list->mvector lst)))

(defun list->column-matrix (&rest lst)
  (mvector->column-matrix (list->mvector lst)))

(defun matrix->array (mat)
  (let*
      ((shape (m:shape mat))
       (number-of-rows (gref shape 0))
       (number-of-cols (gref shape 1))
       (out (make-array shape)))
    (loop for i from 0 below number-of-rows do
      (loop for j from 0 below number-of-cols do
        (setf (aref out i j) (m:tref mat i j))))
    out))

(defmethod g-to-list ((arg m::vector))
  (mvector->list arg))

(defmethod g-to-list ((arg m::matrix))
  (matrix->list arg))

(defmethod g-to-vector ((arg m::vector))
  (mvector->vector arg))

(defmethod add ((arg1 m::abstract-tensor) (arg2 m::abstract-tensor))
  (m:.+ arg1 arg2))


(defmethod negate ((arg1 m::abstract-tensor))
  (* -1 arg1))

(defmethod subtract ((arg1 m::abstract-tensor) (arg2 m::abstract-tensor))
  (+ arg1 (negate arg2)))


(defmethod add ((arg1 m::matrix) (arg2 m::vector))
  (+ (matrix->mvector arg1) arg2))

(defmethod add ((arg2 m::vector) (arg1 m::matrix))
  (+ arg1 arg2))


(defmethod add ((arg1 m::vector) (arg2 list))
  (+ arg1 (list->mvector arg2)))

(defmethod subtract ((arg1 m::vector) (arg2 list))
  (+ arg1 (negate arg2)))

(defmethod add ((arg2 list) (arg1 m::vector))
  (+ arg1 arg2))

(defmethod subtract ((arg2 list) (arg1 m::vector))
  (negate (- arg1 arg2)))

(defmethod add ((arg1 m::matrix) (arg2 list))
  (+ arg1 (mvector->compatible-matrix (list->mvector arg2) arg1)))

(defmethod subtract ((arg1 m::matrix) (arg2 list))
  (+ arg1 (negate arg2)))

(defmethod add ((arg2 list) (arg1 m::matrix))
  (+ arg1 arg2))

(defmethod subtract ((arg2 list) (arg1 m::matrix))
  (negate (- arg1 arg2)))

;; TODO dubious but useful
(defmethod add ((arg1 m::abstract-tensor) (arg2 number))
  (m:.+ arg1 (m:const arg2 (m:shape arg1))))

(defmethod subtract ((arg1 m::abstract-tensor) (arg2 number))
  (+ arg1 (- arg2)))


(defmethod add ((arg2 number) (arg1 m::abstract-tensor))
  (+ arg1 arg2))

(defmethod subtract ((arg2 number) (arg1 m::abstract-tensor))
  (+ (- arg1) arg2))


(defmethod multiply ((arg1 m::matrix) (arg2 m::matrix))
  (m:@ arg1 arg2))

(defmethod multiply ((arg1 m::vector) (arg2 m::vector))
  (m:dot arg1 arg2))

(defmethod multiply ((arg1 m::matrix) (arg2 m::vector))
  (* arg1 (mvector->column-matrix arg2)))

(defmethod multiply ((arg1 m::vector) (arg2 m::matrix))
  (* (mvector->row-matrix arg1) arg2))

(defmethod multiply ((arg1 m::abstract-tensor) (arg2 list))
  (* arg1 (list->mvector arg2)))

(defmethod multiply ((arg1 list) (arg2 m::abstract-tensor))
  (* arg2 arg1))

(defmethod multiply ((arg1 m::abstract-tensor) (arg2 number))
  (m:scale arg1 arg2))

(defmethod multiply ((arg1 number) (arg2 m::abstract-tensor))
  (* arg2 arg1))

(defmethod divide ((arg1 m::abstract-tensor) (arg2 number))
  (* arg1 (/ arg2)))

;; TODO maybe this should raise an error (if arg2 is not just a 1x1 matrix at least)
(defmethod divide ((arg1 number) (arg2 m::abstract-tensor))
  (* arg1 (inv arg2)))

(defmethod divide ((arg1 m::vector) (arg2 m::vector))
  (m:./ arg1 arg2))

(defmethod inv ((arg1 m::matrix))
  (m:inv arg1))

(defmethod divide ((arg1 m::abstract-tensor) (arg2 m::matrix))
  (* (inv arg2) arg1))

(defmethod divide ((arg1 list) (arg2 m::matrix))
  (* (inv arg2) arg1))

(defmethod map (func (vec m::vector) &rest other-vecs)
  (if (and other-vecs (apply #'some (lambda (x) (not (m::vector-p x))) other-vecs))
      (apply #'map (g-to-list vec) (map #'g-to-list other-vecs))
      (let* ((len (length vec))
             (out (zeros len)))
        (loop for i from 0 below len
              do (setf (m:tref out i)
                       (* 1d0
                          (if other-vecs
                              (apply #'funcall
                                     func
                                     (gref vec i)
                                     (map (lambda (v) (gref v i)) other-vecs))
                              (funcall func (gref vec i))))))
        out)))

(defmethod map (func (mat m::matrix) &rest other-mats)
  (if (apply #'some (lambda (x) (not (m::matrix-p x))) other-mats)
      (apply #'map (g-to-list mat) (map #'g-to-list other-mats))
      (let* ((rows (gref (m:shape mat) 0))
             (cols (gref (m:shape mat) 1))
             (out (zeros rows cols)))
        (loop for i from 0 below (gref (m:shape mat) 0)
              do (loop for j from 0 below (gref (m:shape mat) 1)
                       do (setf (m:tref out i j)
                                (if other-mats
                                    (apply #'funcall func (gref mat i j) (map (lambda (v) (gref v i)) other-mats))
                                    (funcall func (gref mat i j))))))
        out)))

(defmethod expt ((arg1 m::abstract-tensor) (arg2 number))
  (map (lambda (x) (expt x arg2))
        arg1))

(defmethod expt ((arg1 number) (arg2 m::abstract-tensor))
  (map (lambda (x) (expt arg1 x))
        arg2))

(defmethod gref ((arg1 m::vector) n &rest other-ns)
  (labels ((transform-negative-index (index)
             (if (<= 0 index)
                 index
                 (+ index (length arg1)))))
    (apply #'m:tref arg1 (transform-negative-index n) (map #'transform-negative-index other-ns))))

(defmethod gref ((arg1 m::matrix) n &rest other-ns)
  (labels ((transform-negative-index (index)
             (if (<= 0 index)
                 index
                 (+ index (length arg1)))))
    (let ((nt (transform-negative-index n))
          (other-ns-t (map #'transform-negative-index other-ns)))
     (if (not other-ns)
         (cond
           ((row-matrix-p arg1) (m:tref arg1 0 nt))
           ((column-matrix-p arg1) (m:tref arg1 nt 0)))
         (apply #'m:tref arg1 nt other-ns-t)))))

(defmethod length ((arg1 m::vector))
  (m:size arg1))

(defmethod length ((arg1 m::matrix))
  (m:size arg1))

(defmethod magnitude ((arg1 m::vector))
  (sqrt (* arg1 arg1)))

(defmethod magnitude ((arg1 m::matrix))
  (assert (or (row-matrix-p arg1) (column-matrix-p arg1)) nil
          "Refusing to take the magnitude of a non-row/column matrix")
  (magnitude (matrix->mvector arg1)))

(defmethod gapply (func (arg m::vector) &rest other-args)
  (if other-args
      (if (cdr other-args)
          (apply func arg other-args)
          (apply func arg (car other-args)))
      (apply func (mvector->list arg))))

(defmethod gapply (func (arg m::matrix) &rest other-args)
  (if other-args
      (if (cdr other-args)
          (apply func arg other-args)
          (apply func arg (car other-args)))
      (apply func (matrix->list arg))))

(defmethod gcons (arg1 (arg2 m::vector))
  (list->mvector (cons arg1 (mvector->list arg2))))

(defmethod gcons (arg1 (arg2 m::matrix))
  (cond
    ((column-matrix-p arg2) (list->column-matrix (gcons arg1 (matrix->list arg2))))
    ((row-matrix-p arg2) (list->row-matrix (gcons arg1 (matrix->list arg2))))
    (t (error "Cannot cons on a matrix unless it is a row- or a column matrix."))))

(defmethod subseq ((arg1 m::vector) start &optional end)
  (list->mvector (subseq (mvector->list arg1) start end)))

(defmethod subseq ((arg1 m::matrix) start &optional end)
  (cond
    ((column-matrix-p arg1)
     (list->column-matrix (subseq (matrix->list arg1) start end)))
    ((row-matrix-p arg1)
     (list->row-matrix (subseq (matrix->list arg1) start end)))
    (t (error "Unable to use subseq on a non-column-/row matrix."))))

;;TODO avoid conversions
(defmethod gsplit ((lst m::vector) parts)
  (list->mvector (gsplit (mvector->list lst) parts)))

(defmethod gsplit ((lst m::matrix) parts)
  (cond
    ((column-matrix-p lst) (list->column-matrix (gsplit (matrix->list lst) parts)))
    ((row-matrix-p lst) (list->row-matrix (gsplit (matrix->list lst) parts)))
    (t (error "I do not know how to split this matrix"))))

(defun condition-number (matrix)
  (let*
      ((eigenvalues (m:eig matrix))
       (max-eigenvalue (apply #'max eigenvalues))
       (min-eigenvalue (apply #'min eigenvalues)))
    (abs (/ max-eigenvalue min-eigenvalue))))

(py4cl:import-module "numpy.linalg" :as "pylinalg" :reload t)

(defun py-condition-number (matrix)
  (pylinalg:cond (matrix->array matrix)))

;; (py-condition-number
;;  ;; #((4.1 2.8) (9.7 6.6))
;;  (m:from-list (list 4.1 2.8 9.7 6.6) '(2 2))
;;  )
