(in-package :scientific)

(defun interpolate (x1 x2 x y1 y2)
  (if
   (= 0 (- x2 x1))
   y1
   (+
    y1
    (*
     (/
      (- y2 y1)
      (- x2 x1))
     (- x x1)))))

(defun sort-two-lists (list1 list2 &optional (pred #'<))
  "Sort two corresponding lists by list1 using pred."
  (let*
      ((zipped-lists (mapfirst #'cons list1 list2))
       (sorted-zipped-lists
         (sort
          zipped-lists
          (lambda (pair1 pair2)
            (funcall pred (first pair1) (first pair2))))))
    (values
     (mapfirst #'first sorted-zipped-lists)
     (mapfirst #'rest sorted-zipped-lists))))

(defun data->func (x-vec y-vec &optional require-sort-p)
  "given x- and y-data as lists, return a function that returns an interpolated value of y for any given x. Use require-sort-p if x-vec is not already monotonically increasing."
  (when require-sort-p
    (multiple-value-setq (x-vec y-vec)
      (sort-two-lists x-vec y-vec)))
    (lambda (x)
      (cond
        ;; ((< x (apply #'min x-vec)) (error "x is too small"))
        ;; ((> x (apply #'max x-vec)) (error "x is too large"))
        ((null (grest x-vec)) 0)         ; TODO handle this case better
        ;; ((< x (first x-vec)) (interpolate (first x-vec) (cadr x-vec) x (first y-vec) (cadr y-vec)))
        ((<= (* (- (first x-vec) x) (- (first (grest x-vec)) x)) 0) (interpolate (first x-vec) (first (grest x-vec)) x (first y-vec) (first (grest y-vec))))
        (t (funcall (data->func (grest x-vec) (grest y-vec)) x)))))


(defun table->func (lst)
  (let*
      ((trans-tab (g-transpose lst))
       (x-vec (gref trans-tab 0))
       (y-vec (gref trans-tab 1)))
    (data->func x-vec y-vec nil)))
