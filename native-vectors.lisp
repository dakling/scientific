(in-package :scientific)

(defmethod add ((arg1 vector) (arg2 vector))
  (map #'+ arg1 arg2))

(defmethod negate ((arg1 vector))
  (* -1 arg1))

(defmethod subtract ((arg1 vector) (arg2 vector))
  (+ arg1 (negate arg2)))


;; TODO dubious but useful
(defmethod add ((arg1 vector) (arg2 number))
  (map (lambda (x) (+ x arg2))
          arg1))

(defmethod subtract ((arg1 vector) (arg2 number))
  (+ arg1 (negate arg2)))

(defmethod add ((arg2 number) (arg1 vector))
  (+ arg1 arg2))

(defmethod subtract ((arg1 number) (arg2 vector))
  (+ arg1 (negate arg2)))

(defmethod multiply ((arg1 vector) (arg2 vector))
  (assert (compatible-p arg1 arg2) nil "Input vectors are not compatible for performing the dot product.")
  (reduce #'+ (map #'* arg1 arg2)))

;; (* (vector 1 2 3) (vector 1 2 3))

(defmethod multiply ((arg1 vector) (arg2 number))
  (map (lambda (x) (* x arg2))
       arg1))

(defmethod multiply ((arg1 number) (arg2 vector))
  (* arg2 arg1))

(defmethod divide ((arg1 vector) (arg2 number))
  (map (lambda (x) (/ x arg2)) arg1 ))

;; (map (lambda (x y z a) (+ x y)) (vector 1 2 3) (vector 3 4 5) (vector 3 4 5) (vector 3 4 5))

(defmethod expt ((arg1 vector) (arg2 number))
  (map (lambda (x) (expt x arg2))
        arg1))

(defmethod expt ((arg1 number) (arg2 vector))
  (map (lambda (x) (expt arg1 x))
        arg2))

(defmethod expt ((arg1 vector) (arg2 vector))
  (cond
    ((= 1 (length arg1)) (expt (first arg1) arg2))
    ((= 1 (length arg2)) (expt arg1 (first arg2)))
    ((= (length arg1) (length arg2)) (map #'expt arg1 arg2))))

;; TODO nested vectors
(defmethod gref ((arg1 vector) n &rest other-ns)
  (if other-ns
      (apply #'gref (gref arg1 n) other-ns)
      (if (> 0 n)
          (elt arg1 (+ (length arg1) n))
          (elt arg1 n))))

(defmethod magnitude ((arg1 vector))
  (sqrt (* arg1 arg1)))

(defmethod g-to-list ((arg vector))
  (coerce arg 'list))

(defmethod g-to-vector ((arg vector))
  arg)

(defmethod compatible-p ((arg1 vector) (arg2 vector))
  (=
   (length arg1)
   (length arg2)))
