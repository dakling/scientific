;;;; package.lisp

(defpackage :scientific
  ;; (:use #:static-dispatch-cl)
  (:use #:generic-cl)
  (:shadowing-import-from
   :generic-cl.math
   :expt
   :sqrt)
  (:local-nicknames
   (#:m #:magicl)
   (#:a #:alexandria)
   (#:s #:serapeum))
  (:export
   :gref
   :gapply
   :magnitude
   :gcons
   :gsplit
   :gdrop-left
   :gdrop-right
   :g-flatten
   :g-transpose
   :grest
   :zeros
   :ones
   :unit-list-scaled
   :gen-matrix
   :gen-vector
   :mvector->list
   :list->mvector
   :vector->row-matrix
   :vector->column-matrix
   :vector->compatible-matrix
   :matrix->vector
   :matrix->list
   :foreign-array-p
   :foreignvector->list
   :range
   :diff
   :jacobian-fds
   :newton
   :nelder-mead
   :broyden
   :fletcher-reeves
   :hybrid-scaled
   :hybrid-unscaled
   :discrete-newton
   :scipy-root
   :scipy-hybr
   :scipy-krylov
   :scipy-broyden
   :scipy-df-sane
   :scipy-anderson
   :scipy-levenberg-marquardt
   :mgl-minimize
   :mgl-cg-minimize
   :mgl-sgd-minimize
   :make-nonuniform-grid
   :make-uniform-grid
   :make-exponential-grid-left
   :make-exponential-grid-right
   :make-exponential-grid-both
   :grid-number-of-nodes
   :table->ode-solution
   :ode-solution->list
   :ode-solution->func
   :ode-solution->bvp-initial-guess
   :make-ode-solution
   :ode-solution-grid
   :ode-solution-solution
   :ode-solution-name
   :ode-solution-p
   :bvp-output->ode-solution
   :map-ode-solution-to-grid
   :write-ode-solution-to-file
   :read-ode-solution-from-file
   :fdm-discretizer
   :explicit-euler-step
   :implicit-euler-step
   :crank-nicholson-step
   :timestepper
   :adaptive-pseudo-timestepper
   :solve-bvp-system
   :py-solve-bvp-system
   :integrate
   :parse-file
   :data->func
   :table->func
   :plot
   :plot-ode-solution
   :plot-grid
   :gnuplot
   :matplotlib))
