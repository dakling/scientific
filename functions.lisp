(in-package :scientific)

(defmethod +-bin ((arg1 function) (arg2 function))
  (lambda (&rest args)
    (+
     (gapply arg1 args)
     (gapply arg2 args))))

(defmethod --bin ((arg1 function) (arg2 function))
  (lambda (&rest args)
    (-
     (gapply arg1 args)
     (gapply arg2 args))))

;; TODO carry on with this

(defmethod gcons ((arg1 function) (arg2 function))
  (lambda (&rest args)
    (gcons
     (gapply arg1 args)
     (gapply arg2 args))))

;; (gapply (gcons (lambda (&rest x) (nth 0 x)) (lambda (&rest x) (nth 1 x))) 2 3)

(defmethod g-to-list ((arg function))
  (list arg))
