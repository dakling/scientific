(in-package :scientific)

(defun list->mgl-mat (lst)
  (mgl:make-mat (length lst)
                :initial-contents lst))

(defun mgl-mat->list (mat)
  (g-to-list (mgl-mat:mat-to-array mat)))

(defmethod g-to-list ((mat mgl:mat))
  (mgl-mat->list mat))
